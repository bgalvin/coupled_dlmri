function [ combinedRCIC_clean ] = complex_col2im( rebuild_clean, rsize, pshape, sshape  )
% complex_im2col(matrix, [s1 s2 s3], [n1 n2 n3], [m1 m2 m3] ))
% s - orig matrix size
% n - block size
% m - step size

outRC_clean = real(rebuild_clean);
outIC_clean = imag(rebuild_clean);

reoutRC_clean = col2imstep(outRC_clean, rsize, pshape, sshape);
reoutIC_clean = col2imstep(outIC_clean, rsize, pshape, sshape);

cnt = countcover(rsize, pshape, sshape);

reoutRC_clean = reoutRC_clean./cnt;
reoutIC_clean = reoutIC_clean./cnt;

combinedRCIC_clean = complex(reoutRC_clean, reoutIC_clean);

end

