function [ complexrows ] = complex_im2col( kspace_nav, pshape, sshape )
% complex_im2col(matrix, [n1 n2 n3], [m1 m2 m3] ))
% n - block size
% m - step size

ks_real = real(kspace_nav);
ks_imag = imag(kspace_nav);

RC = im2colstep(ks_real, pshape, sshape);
IC = im2colstep(ks_imag, pshape, sshape);

complexrows = complex(RC,IC);

end

