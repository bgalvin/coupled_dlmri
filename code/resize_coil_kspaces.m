%clear all
close all

%load('D:\git\coupledDLMRI\data\MID227_coils')

% fname = strcat('D:\git\coupledDLMRI\results\twNav_bwAvg_test_nc',...
%     num2str(nc),...
%     '_ds_250_ps_16_L_25.mat')

prefixes = ['nav'; 'ext'; 'avg']

for i = 1:length(prefixes)
    
    prefix =  prefixes(i,:);
    
    for nc = 1:5
        
        varname = strcat('kspace_',prefix,'_coil',num2str(nc))
        toResize = eval(varname);
        newSize = resize(toResize,[256 198 60]);
    
        assignin('base', strcat(varname,'_res'), newSize)
    
    end   
end

