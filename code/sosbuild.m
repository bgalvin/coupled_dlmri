%close all
clear all
addpath('utils\','sparseApprox\','snrAndDisplay\')
%MID281_allcoil_erfl1_nc4_TW_kspace_nav1_BW_kspace_avg1_ds_500_ps_16_L_100.mat has
%MID281_allcoil_erfl1_nc1_TW_kspace_nav1_BW_kspace_avg1_ds_500_ps_16_L_100
%MID227_allcoil_doublestep_nc1_TW_kspace_nav_BW_kspace_avg_ds_650_ps_16_L_120
%MID227_resized_allcoil_reversed_nc5_TW_kspace_avg_BW_kspace_nav_ds_400_ps_16_L_120
%MID227_resized_allcoil_singlestep_nc5_TW_kspace_nav_BW_kspace_avg_ds_650_ps_16_L_240
%MID227_resized_preserveDCATOM_nc5_TW_kspace_nav_BW_kspace_avg_ds_800_ps_8_L_20
%MID227_resized_allcoil_singlestep_nc4_TW_kspace_nav_BW_kspace_avg_ds_650_ps_5_L_25

% firstpart = 'MID227_resized_allcoil_singlestep'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_650_ps_16_L_240'
% firstpart = 'MID227_resized_allcoil'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_600_ps_16_L_120'
% firstpart = 'MID227_resized_allcoil_reversed'
% secondpart = 'TW_kspace_avg_BW_kspace_nav_ds_400_ps_16_L_120';
% MID227_resized_sos_kspace_nc4_TW_kspace_nav_BW_kspace_avg_ds_800_ps_8_L_15
% firstpart = 'MID227_resized_sos_kspace_chol_small'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_1000_ps_8_L_50';
%MID227_resized_sos_kspace_chol_small_nc5_TW_kspace_nav_BW_kspace_avg_ds_1000_ps_8_L_50

%twNav_bwAvg_test_nc5_ds_250_ps_16_L_25
%MID227_resized_allcoil_singlestep_nc4_TW_kspace_nav_BW_kspace_avg_ds_650_ps_5_L_25
% firstpart = 'MID227_resized_allcoil_singlestep'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_650_ps_5_L_25';

%MID227_resized_allcoil_reversed_nc5_TW_kspace_avg_BW_kspace_nav_ds_400_ps_16_L_120
% firstpart = 'MID227_resized_allcoil_reversed'
% secondpart = 'TW_kspace_avg_BW_kspace_nav_ds_400_ps_16_L_120';

%MID227_resized_allcoil_singlestep_nc4_TW_kspace_nav_BW_kspace_avg_ds_650_ps_16_L_240
% firstpart = 'MID227_resized_allcoil_singlestep'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_650_ps_16_L_240';

%MID227_resized_shift_dict_nc4_TW_kspace_nav_BW_kspace_avg_ds_5000_ps_4_L_15
% firstpart = 'MID227_resized_shift_dict'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_5000_ps_4_L_15';

%MID227_resized_smallshift_dict_nc2_TW_kspace_avg_BW_kspace_nav_ds_5000_ps_4_L_15
% firstpart = 'MID227_resized_smallshift_dict'
% secondpart = 'TW_kspace_avg_BW_kspace_nav_ds_5000_ps_4_L_15';

% MID227_resized_new_opts_nc1_TW_kspace_avg_BW_kspace_nav_ds_500_ps_8_L_15
% firstpart = 'MID227_resized_new_opts_div1'
% secondpart = 'TW_kspace_avg_BW_kspace_nav_ds_500_ps_4_L_15';

%MID227_resized_new_opts_div1_rightdirection_nc5_TW_kspace_nav_BW_kspace_avg_ds_2500_ps_8_L_25
% firstpart = 'MID227_resized_new_opts_div1_rightdirection'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_2500_ps_8_L_25';

%MID227_resized_more_opts_nc1_TW_kspace_nav_BW_kspace_avg_ds_2000_ps_8_L_50
% firstpart = 'MID227_resized_more_opts'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_2000_ps_8_L_50';

%MID227_resized_more_opts_nc3_TW_kspace_nav_BW_kspace_avg_ds_600_ps_16_L_10
% firstpart = 'MID227_resized_more_opts'
% secondpart = 'TW_kspace_nav_BW_kspace_avg_ds_2000_ps_8_L_50';

filenames = 'MID281_final_nc1_TW_kspace_nav_BW_kspace_avg_ds_1000_ps_8_L_24';
filenames = 'MID227_resized_2d_final_nonorm_means_nc1_TW_kspace_nav_BW_kspace_avg_ds_500_ps_4_L_10';
firstsplit = strsplit(filenames,'_nc');
secondSplit =  strsplit(firstsplit{2},'_TW');

% nc =4;
% fname = strcat('../results/',...
%     firstsplit{1},...
%     '_nc',...
%     num2str(nc),...
%     '_TW',...
%     secondSplit{2});
% load(fname,'recon_dAVG_gNAVNAV')


for nc = [1 2 3 4 5]
    
    fname = strcat('../results/',...
        firstsplit{1},...
        '_nc',...
        num2str(nc),...
        '_TW',...
        secondSplit{2});
    
    load(fname)
    
    %     MID227_resized_new_opts_div1_rightdirection_nc3_TW_kspace_nav_BW_kspace_avg_ds_2500_ps_8_L_25
    %     MID281_mallerstep_nc3_TW_kspace_nav_BW_kspace_avg_ds_2000_ps_4_L_30
    %     MID281_mallerstep_nc1_TW_kspace_nav_BW_kspace_avg_ds_2000_ps_4_L_30
    %         filenames1 = 'MID281_biggerstep_nc5_TW_kspace_nav_BW_kspace_avg_ds_2000_ps_8_L_30';
    %         firstsplit1 = strsplit(filenames1,'_nc');
    %         secondSplit1 = strsplit(firstsplit1{2},'_TW');
    %
    %         fname1 = strcat('../results/',...
    %             firstsplit1{1},...
    %             '_nc',...
    %             num2str(nc),...
    %             '_TW',...
    %             secondSplit1{2});
    %
    %         load(fname1,'recon_NAV','recon_AVG')
    %         recon_NAV = recon_clean;
    %         recon_AVG = recon_mov;
    %
    
    if nc == 1
        toAdd = {recon_NAV,recon_AVG,...
            recon_dAVG_gAVGNAV,recon_dAVG_gNAVAVG,recon_dAVG_gNAVNAV,recon_dNAV_gAVGAVG,recon_dNAV_gNAVAVG,recon_dNAV_gNAVNAV...
            recon_dNAV_gNAVAVG_avg_pmeans};
        toAdd = cellfun(@(x){abs(x).^2},toAdd);
    else
        toAdd2 = {recon_NAV,recon_AVG,...
            recon_dAVG_gAVGNAV,recon_dAVG_gNAVAVG,recon_dAVG_gNAVNAV,recon_dNAV_gAVGAVG,recon_dNAV_gNAVAVG,recon_dNAV_gNAVNAV...
            recon_dNAV_gNAVAVG_avg_pmeans};
        toAdd = cellfun(@(x,y){x + abs(y).^2},toAdd, toAdd2);
    end
    
end

toAdd = cellfun(@(x){sqrt(x)},toAdd);
names = {'recon_clean','recon_mov',...
    'recon_dAVG_gAVGNAV','recon_dAVG_gNAVAVG','recon_dAVG_gNAVNAV','recon_dNAV_gAVGAVG','recon_dNAV_gNAVAVG','recon_dNAV_gNAVNAV','werid'};



for i=1:1:length(toAdd)
    for ii=1:size(toAdd{i},3)
        toAdd{i}(:,:,ii)=rot90(toAdd{i}(:,:,ii),2);
    end
end

if usejava('jvm') && ~feature('ShowFigureWindows')
else
    %slice 6 has a nice blood vessel on the right side that moves between
    %avg and nav
    chooseSlice  = 10;
    
    displayMulitiple(toAdd,names, chooseSlice)
    
    ssimMat = [];
    diffMat = []
    for i = 1:size(toAdd,2)
        for j = 1:size(toAdd,2)
            diffMat(i,j) = norm(toAdd{i}(:)-toAdd{j}(:));
            ssimMat(i,j) = get_ssim(toAdd{i},toAdd{j},chooseSlice );
        end
    end
    
end

