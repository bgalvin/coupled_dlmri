close all
clear all
clc

warning off
load('./data/data.mat')

recon_mov12 = ifftshift(ifftn(kspace_avg));
kspace_avg1 = (fftn(ifftshift(recon_mov12(:,:,15))));

recon_nav12 = ifftshift(ifftn(kspace_nav));
kspace_nav1 = (fftn(ifftshift(recon_nav12(:,:,15))));

outdsize = []
outpsize = [] 
outpsparsec = [] 
% for dsize = 100:100:500
% [recon_mov_snr, recon_clean_snr, rerecon_mov_snr, rerecon_clean_snr, recon_cup_snr, rerecon_clean_wavg_snr ] ...
%     =   split_DL_rebuild_functionize( kspace_nav1, kspace_avg1, 5, 1, dsize,      15,          .001,       10, 'teste' );    
%         %split_DL_rebuild_functionize( TW,         BW,         ps,ss, dictsize, sparseconst, errorconst,iters, filetitle )
% outdsize = [outdsize; recon_mov_snr, recon_clean_snr, rerecon_mov_snr, rerecon_clean_snr, recon_cup_snr, rerecon_clean_wavg_snr]   
% end

for psize = 5:5:20
[recon_mov_snr, recon_clean_snr, rerecon_mov_snr, rerecon_clean_snr, recon_cup_snr, rerecon_clean_wavg_snr ] ...
    =   split_DL_rebuild_functionize( kspace_nav1, kspace_avg1, psize, 1, 150,      5,          .001,       10, 'teste' );    
        %split_DL_rebuild_functionize( TW,         BW,         ps,ss, dictsize, sparseconst, errorconst,iters, filetitle )
outpsize = [outpsize; recon_mov_snr, recon_clean_snr, rerecon_mov_snr, rerecon_clean_snr, recon_cup_snr, rerecon_clean_wavg_snr]     
close all
end


for sparsec = 5:10:35
[recon_mov_snr, recon_clean_snr, rerecon_mov_snr, rerecon_clean_snr, recon_cup_snr, rerecon_clean_wavg_snr ] ...
    =   split_DL_rebuild_functionize( kspace_nav1, kspace_avg1, 7, 1, 150,      sparsec,          .001,       10, 'teste' );    

        %split_DL_rebuild_functionize( TW,         BW,         ps,ss, dictsize, sparseconst, errorconst,iters, filetitle )
outpsparsec = [outpsparsec; recon_mov_snr, recon_clean_snr, rerecon_mov_snr, rerecon_clean_snr, recon_cup_snr, rerecon_clean_wavg_snr]     
close all
end