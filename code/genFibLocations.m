function [filtX, filtY] = genFibLocations(number,patchsize, imsizex, imsizey)
% 
% patchsize = 4;
% imsizex = 256;
% imsizey = 198;

cirsizex = imsizex*sqrt(2)
cirsizey = imsizey*sqrt(2)

[x y] = fibonacci_spiral(number);
x = round( x./max(x)*cirsizex + .5*imsizex );
y = round(y./max(y)*cirsizey + .5*imsizey);

xInds = find(x>ceil(patchsize/2) & x < imsizex-floor(patchsize/2));
yInds = find(y>ceil(patchsize/2) & y < imsizey-floor(patchsize/2));
toUseInds = intersect(xInds,yInds);
filtX = x(toUseInds);
filtY = y(toUseInds);
% scatter(filtX,filtY,'b.');
% hold on
% for i=1:1:length(filtY)
% %          param................... 1x5 array
% %          - param = [a, b, w, h, theta]
% %          - (a,b): the center of the rectangle
% %          - (w,h): width and height of the rectangle > 0
% %          - theta: the rotation angle of the rectangle
% %          style................... string
% %          - plot style string
% 
% DrawRectangle([filtX(i) filtY(i) patchsize patchsize 0]); 
% end
% axis tight

length(filtY)