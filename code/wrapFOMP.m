function [compiledX] = wrapFOMP( A, b, k, errFcn, opts )
% Simply allows for OMP to process multiple signals in b.
%
% x = wrapFOMP( A, b, k )
%   uses the Orthogonal Matching Pursuit algorithm (OMP)
%   to estimate the solution to the equation
%       b = A*x     (or b = A*x + noise )
%   where there is prior information that x is sparse.
%
%   "A" may be a matrix, or it may be a cell array {Af,At}
%   where Af and At are function handles that compute the forward and transpose
%   multiplies, respectively.
%
% [x,r,normR,residHist,errHist] = OMP( A, b, k, errFcn, opts )
%   is the full version.
% Outputs:
%   'x' is the k-sparse estimate of the unknown signal
%   'r' is the residual b - A*x
%   'normR' = norm(r)
%   'residHist'     is a vector with normR from every iteration
%   'errHist'       is a vector with the outout of errFcn from every iteration
%
% Inputs:
%   'A'     is the measurement matrix
%   'b'     is the vector of observations
%   'k'     is the estimate of the sparsity (you may wish to purposefully
%              over- or under-estimate the sparsity, depending on noise)
%              N.B. k < size(A,1) is necessary, otherwise we cannot
%                   solve the internal least-squares problem uniquely.
%
%   'k' (alternative usage):
%           instead of specifying the expected sparsity, you can specify
%           the expected residual. Set 'k' to the residual. The code
%           will automatically detect this if 'k' is not an integer;
%           if the residual happens to be an integer, so that confusion could
%           arise, then specify it within a cell, like {k}.
%
%   'errFcn'    (optional; set to [] to ignore) is a function handle
%              which will be used to calculate the error; the output
%              should be a scalar
%
%   'opts'  is a structure with more options, including:
%       .printEvery = is an integer which controls how often output is printed
%       .maxiter    = maximum number of iterations
%       .slowMode   = whether to compute an estimate at every iteration
%                       This computation is slower, but it allows you to
%                       display the error at every iteration (via 'errFcn')
%
%       Note that these field names are case sensitive!
%
% Ben Galvin 5/2014 ~ removed some print commands for speed.

compiledX = zeros(size(A,2),size(b,2));
%s = matlabpool('size')

if opts.runCHOL
    
    Dgram = A'*A;
    %DTx = A'*b;
    
    parfor idx = 1:size(b,2)
        DTx = A'*b(:,idx);
        %compiledX(:,idx) =ompdChol(b(:,idx),A,Dgram,DTx(:,idx),k,1e-16);
        compiledX(:,idx) =ompdChol(b(:,idx),A,Dgram,DTx,k,1e-16);
    end
    
else
    parfor idx = 1:size(b,2)
        
        curb = b(:,idx)
        
        if opts.ignoreCorrupted
            ip = (curb ~= 0)
            bspec = curb(ip)
            Aspec = A(ip,:)
            modK = min([k,sum(ip(:))]);
            compiledX(:,idx) = fastOMP( Aspec,bspec, modK,errFcn,opts);
        else
            compiledX(:,idx) = fastOMP( A,curb, k,errFcn,opts);
        end
        
    end
end