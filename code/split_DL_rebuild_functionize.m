function fname  = split_DL_rebuild_functionize( TW, BW, ps, ss, dictsize, sparseconst, errorconst,iters, filetitle )
%profile on
%warning off
%%%%%%%%%%%%%



%%%%%%%%%%%%%%%%%%%%
% Loading and Path %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

addpath('./utils')
rng(5) %repeatibility



%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm Parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% patch size - params.pshape
% patch skip - params.sshape

if length(size(TW)) > 2
    disp('using 3d patches')
    params.pshape = [ps ps ps];
    params.sshape = [ss ss ss];
else
    disp('using 2d patches')
    params.pshape = [ps ps];
    params.sshape = [ss ss];
end

if length(ss) > 1
    disp('using non-square patches')
    params.pshape =  ps;
    params.sshape = ss;
end

% display progress?
params.displayProgress = 1 ;

% dictionary size
params.K = dictsize;

% KSVD iterations
params.numIteration =iters;

% Determins the use of L or ERRORGOAL, below...
params.errorFlag =0;
%  0 - fix number of coefficients is used for representation of each signal. param.L
params.L =sparseconst;
%  1 - until a specific representation error is reached. param.errorGoal
params.errorGoal = errorconst;

params.preserveDCAtom = 0;
params.InitializationMethod = 'DataElements';
params.InitializationMethod = 'GivenMatrix';

%pruning threshold, the dict. is initialized on a randperm of these after pruning

%varThresh =  12002/4;

params.orisize = size(TW);


twstr = inputname(1);
bwstr = inputname(2);

params.fname = sprintf('%s_TW_%s_BW_%s_ds_%d_ps_%d_L_%d.mat',filetitle,twstr,bwstr,params.K ,params.pshape(1),params.L );

params



%%%%%%%%%%%%%%
% Algorithm  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('starting algo')

kspace_nav = TW;
kspace_avg = BW;

tic;

%%% Normalize kspaces

maxNav = max(max(max(kspace_nav)));
maxAvg = max(max(max(kspace_avg)));
% maxNav = max(max(max(ifftn(kspace_nav))));
% maxAvg = max(max(max(ifftn(kspace_avg))));


%%% move NAV to rows, KSVD, and rebuild nav

rows_NAV = complex_im2col( kspace_nav./maxNav, params.pshape, params.sshape );


%%% remove low var atoms

pvars = var(rows_NAV, 0, 1);
varThresh = mean(pvars)/2;
iniDict = rows_NAV(:, pvars > varThresh);
toUseIdx = randperm(size(iniDict,2));
params.initialDictionary = rows_NAV(:,toUseIdx(1:params.K));


[dict_NAV, opts] = KSVDC2(rows_NAV, params, params.errorGoal);
im_NAV = complex_col2im( dict_NAV*opts.CoefMatrix, params.orisize, params.pshape, params.sshape)*maxNav;


%%% move AVG to rows, OMP and rebuild AVG

rows_AVG = complex_im2col( kspace_avg./maxAvg, params.pshape, params.sshape );
%gam_AVG = OMP(dict_NAV,rows_AVG, params.L);
%gam_AVG = OMPerr(dict_NAV,rows_AVG,params.errorGoal);
%gam_AVG = OMPerrn(dict_NAV, rows_AVG, params.errorGoal, params.L);
fompopts.slowMode = 0;
fompopts.printEvery = 0;
fompopts.ignoreCorrupted = 0;
gam_AVG = wrapFOMP( dict_NAV, rows_AVG, params.L, [] , fompopts);
im_AVG = complex_col2im( dict_NAV*gam_AVG , params.orisize , params.pshape , params.sshape)*maxAvg;


%%% coupled Dict step

gam_NAV = opts.CoefMatrix;
dict_AVG = rows_AVG*gam_NAV'/(gam_NAV*gam_NAV');
im_gamNAV_dictAVG = complex_col2im( dict_AVG*gam_NAV, params.orisize, params.pshape, params.sshape)*maxNav;
%cup_gamNAV_dictAVG = complex_col2im( dict_NAV*gam_AVG, params.orisize, params.pshape, params.sshape)*maxNav;

gam_NAV_wAVG = wrapFOMP( dict_AVG, rows_NAV, params.L, [] , fompopts);
im_NAV_wAVG = complex_col2im( dict_AVG*gam_NAV_wAVG , params.orisize , params.pshape , params.sshape)*maxAvg;

%recombed = (cup_gamNAV_dictAVG+im_NAV)/2;

rerecon_mov = ifftshift(ifftn(im_AVG));
recon_mov = ifftshift(ifftn(kspace_avg));
rerecon_clean = ifftshift(ifftn(im_NAV));
recon_clean = ifftshift(ifftn(kspace_nav));
recon_cup = ifftshift(ifftn(im_gamNAV_dictAVG));
recon_NAV_wAVG = ifftshift(ifftn(im_NAV_wAVG));


%%%%%%%%%%%%%
% End Stuff %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fname = params.fname;

params.totaltime = toc;

sendAnEmail(fname, params)

save(strcat('../results/',fname))



