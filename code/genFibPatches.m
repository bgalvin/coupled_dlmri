function [ incols] = genFibPatches( kspace,number,patchsize,imagesize )
%GENFIBPATCHES Summary of this function goes here
%   Detailed explanation goes here

[xlocs, ylocs] = genFibLocations(number,patchsize, imagesize(1), imagesize(2));

xsize = ceil(patchsize/2)-1 ;
ysize = floor(patchsize/2);

incols = [];
for i=1:1:length(xlocs)
    toSamp = zeros(size(kspace));
    for xs = xlocs(i)-xsize:1:xlocs(i)+ysize
        for ys = ylocs(i)-xsize:1:ylocs(i)+ysize
            toSamp(xs,ys) = true;
        end
    end
    

    incols(:,exxnd+1) = kspace(toSamp==1);
end



end

