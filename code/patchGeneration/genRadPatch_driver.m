clear all
%close all
clc

addpath('exportFig')
load('../../data/data.mat','kspace_nav','kspace_avg');

image_orig = fftshift(ifftn(fftshift(kspace_avg)));
image_orig = fftshift(fftn(fftshift(image_orig(:,:,10))));

image_orig = center_kspace(image_orig);

recon = fftshift(ifftn(fftshift(image_orig)));

%image_orig = randn(imxsize,imysize)
%image_orig = mat2gray(imread('cameraman.tif'));


imSize = size(image_orig);
rInc = .5;
tInc = .5;

[outCols centered_xPol centered_yPol] = genRadPatch_func(image_orig,rInc,tInc);

[outCols] = genImgFromRadPatch_func(imSize, outCols,rInc,tInc );

descalefactor = @(x) sign(x).*x.^(1/2);

outCols_crop_todisp = descalefactor(outCols);
image_orig_todisp = descalefactor(image_orig);

figure
imagesc(abs(recon));
colormap(gray)
brighten(.5)
title('Original Image')
axis tight
export_fig('im_orig.pdf', '-a1', '-pdf','-q100','-painters');

figure
imagesc(abs(image_orig_todisp),[min(min(min(abs(image_orig_todisp)))) max(max(max(abs(image_orig_todisp))))])
title('Original Kspace')
export_fig('ks_orig.pdf', '-a1', '-pdf','-q100','-painters');

figure
scatter(centered_xPol,centered_yPol,5,'.')
title('Polar Sampling Pattern')
export_fig('ks_samp_pattern.pdf', '-a1', '-pdf','-q100','-painters');

figure
imagesc(abs(outCols_crop_todisp),[min(min(min(abs(image_orig_todisp)))) max(max(max(abs(image_orig_todisp))))])
title('Reconstructed Kspace')
axis tight
export_fig('ks_samped.pdf', '-a1', '-pdf','-q100','-painters');

figure
recon_interpd = fftshift(ifftn(fftshift(outCols)));
imagesc(abs(recon_interpd),[min(min(min(abs(recon)))) max(max(max(abs(recon))))]);
colormap(gray)
title('Reconstructed Image')
brighten(.5)
export_fig('im_samped.pdf', '-a1', '-pdf','-q100','-painters');

