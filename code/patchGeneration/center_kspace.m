% clear all
% close all
% clc
% 
% load('../../data/data.mat','kspace_nav','kspace_avg');
% 
% 
% im_o = fftshift(ifftn(fftshift(kspace_nav)));
% ks_c = fftshift(fftn(fftshift(im_o(:,:,20))));
% 

function [ ks_c_m ] = center_kspace(ks_c)
im_c = fftshift(ifftn(fftshift(ks_c)));
ims = size(ks_c);

[~,ind] = max(ks_c(:));
[m,n] = ind2sub(size(ks_c),ind);

ind_together = [m,n];
diff = size(ks_c)/2 - ind_together;

ny = diff(1);
nx = diff(2);

w1 = ([-ims(1)/2:ims(1)/2-1]*pi)/(ims(1)/2);
w2 = ([-ims(2)/2:ims(2)/2-1]*pi)/(ims(2)/2);

yps(:,1) = exp(  w1.*(1i*ny) );
xps(1,:) = exp(  w2.*(1i*nx) );
ypsmat = repmat(yps,[1,ims(2)]);
xpsmat = repmat(xps,[ims(1),1]);
im_c_m = im_c.*ypsmat.*xpsmat;

ks_c_m = fftshift(fftn(fftshift(im_c_m)));

% 
% figure
% imagesc(abs(im_c))
% figure
% imagesc(abs(ks_c))
% 
% figure
% imagesc(abs(im_c_m))
% figure
% imagesc(abs(ks_c_m))
% yps(:,1,1) = exp(  (1:1:ims(1)).*(1i*y(i)) );
% xps(1,:,1) = exp(  (1:1:ims(2)).*(1i*x(i)) );
% zps(1,1,:) = exp(  (1:1:ims(3)).*(1i*z(i)) );
% ypsmat = repmat(yps,[1,ims(2),ims(3)]);
% xpsmat = repmat(xps,[ims(1),1,ims(3)]);
% zpsmat = repmat(zps,[ims(1),ims(2),1]);
% imspace = imspace_nav.*ypsmat.*xpsmat.*zpsmat + sig*complex(randn(ims),randn(ims));
