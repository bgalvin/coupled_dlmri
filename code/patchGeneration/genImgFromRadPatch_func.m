function [ out_img ] = genImgFromRadPatch_func(imsize, inCols,rInc,tInc )
%GENIMGFROMRADPATCH_FUNC Summary of this function goes here
%   Detailed explanation goes here

r = [0:rInc:min(imsize)/2];
theta = [0:tInc:359]*pi/180;
[tPol, rPol] =meshgrid(theta,r);
[xPol,yPol] = pol2cart(tPol(:),rPol(:));
centered_yPol = yPol+imsize(1)/2;
centered_xPol = xPol+imsize(2)/2;

F_grid = []

for i = 1:size(inCols,2)
    F_grid = cat(1,F_grid, inCols(:,i));
end

%take it back
F_scat = scatteredInterpolant(centered_xPol, centered_yPol, F_grid,'natural','none');
[x, y ] = meshgrid(1:imsize(2), 1:imsize(1));
out_img = F_scat(x,y);
out_img(isnan(out_img))=0+0i;

end

