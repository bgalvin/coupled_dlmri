function [inCols centered_xPol centered_yPol]= genRadPatch_func(image_orig,rInc,tInc)
%genRadPatch_func Summary of this function goes here
%   Detailed explanation goes here

%get image size
im_orig_size = size(image_orig);

%pad for interp
padsize = 0;
image = padarray(image_orig,[padsize padsize],'replicate');
imsize = size(image);

% dont need all datapoints but is easier this way
%[Xcart Ycart] = ind2sub(size(image),[1:imsize(1)*imsize(2)]);
minorAxis = find(imsize == min(imsize));
majorAxis = find(imsize == max(imsize));

% spec desired samples in polar coords
inCols = []
for theta=((0:tInc:359)*pi/180);
%theta = 0*pi/180
r = [0:rInc:min(imsize-padsize)/2];
[tPol, rPol] =meshgrid(theta,r);

% convert desired samps into cartesian and center (polar points go neg in
% cart)
[xPol,yPol] = pol2cart(tPol(:),rPol(:));
centered_yPol = yPol+imsize(1)/2;
centered_xPol = xPol+imsize(2)/2;
F_grid = interp2(image, centered_xPol, centered_yPol,'linear');

inCols(:,end+1) = F_grid;


end

end
