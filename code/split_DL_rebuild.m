clear all
close all
clc
%profile on


matlabpool

%%%%%%%%%%%%%%%%%%%%
% Loading and Path %
%%%%%%%%%%%%%%%%%%%%

addpath('./utils')
load('./data/data.mat')
% load('./data/tiny_clean.mat')
% load('./data/tiny_rand.mat')

%forcirc
% kspace_nav = ifftn(clean_kspace);
% kspace_avg = ifftn(kspace);
% kspace_nav = complex(randn(10,10,10),randn(10,10,10));
% kspace_avg = complex(randn(10,10,10),randn(10,10,10));

% sizeratios = [ 0.1563 .2 .67];
% orisize = size(kspace_avg)
% desiredsize =[80 80 80]; %150 100 30]
% sratio = desiredsize./orisize
% %sratio = orisize*1
%
% rskspaceAvg = resize(kspace_avg,size(kspace_avg).*sratio);
% rskspaceNav = resize(kspace_nav,size(kspace_nav).*sratio);

recon_mov12 = ifftshift(ifftn(kspace_avg));
kspace_avg1 = (fftn(ifftshift(recon_mov12(:,:,15))));

recon_nav12 = ifftshift(ifftn(kspace_nav));
kspace_nav1 = (fftn(ifftshift(recon_nav12(:,:,15))));



%%%%%%%%%%%%%%%%%%%%%%%%
% Algorithm Parameters %
%%%%%%%%%%%%%%%%%%%%%%%%

% patch size
params.pshape = [5 5];

% patch skip
params.sshape = [1 1];

% display progress?
params.displayProgress = 1 ;

% dictionary size
params.K = 500;

% KSVD iterations
params.numIteration =10;

% Determins the use of L or ERRORGOAL, below...
params.errorFlag =0;
%  0 - fix number of coefficients is used for representation of each signal. param.L
params.L =15;
%  1 - until a specific representation error is reached. param.errorGoal
params.errorGoal = .001;

params.preserveDCAtom = 0;
params.InitializationMethod = 'DataElements';

TW = 'kspace_nav1';
BW = 'kspace_avg1';
% TW = 'rskspaceAvg';
% BW = 'rskspaceNav';


%%%%%%%%%%%%%%
% Algorithm  %
%%%%%%%%%%%%%%

kspace_nav = eval(TW);
kspace_avg = eval(BW);

fname = sprintf('TW_%s_BW_%s_ds_%d_ps_%d_L_%d.mat',TW,BW,params.K ,params.pshape(1),params.L )

params

disp('starting algo')

tic;
% get orig size
rsize = size(kspace_avg);

% Normalize kspaces
maxNav = max(max(max(ifftn(kspace_nav))));
maxAvg = max(max(max(ifftn(kspace_avg)))); %peak intensity estimated from zero-filled result

% move NAV to rows, KSVD, and rebuild nav
rows_NAV = complex_im2col( kspace_nav./maxNav, params.pshape, params.sshape );
[dict_NAV, opts] = KSVDC2(rows_NAV, params, params.errorGoal);
im_NAV = complex_col2im( dict_NAV*opts.CoefMatrix, rsize, params.pshape, params.sshape)*maxNav;

% move AVG to rows, OMP and rebuild AVG
rows_AVG = complex_im2col( kspace_avg./maxAvg, params.pshape, params.sshape );
%gam_AVG = OMP(dict_NAV,rows_AVG, params.L);
%gam_AVG = OMPerr(dict_NAV,rows_AVG,params.errorGoal);
%gam_AVG = OMPerrn(dict_NAV, rows_AVG, params.errorGoal, params.L);
opts.slowMode = 0;
opts.printEvery = 0;
gam_AVG = wrapFOMP( dict_NAV, rows_AVG, params.L, [] , opts);
im_AVG = complex_col2im( dict_NAV*gam_AVG , rsize , params.pshape , params.sshape)*maxAvg;

% coupled Dict step
gam_NAV = opts.CoefMatrix;
dict_AVG = rows_AVG*gam_NAV'*inv(gam_NAV*gam_NAV');
im_gamNAV_dictAVG = complex_col2im( dict_AVG*gam_NAV, rsize, params.pshape, params.sshape)*maxNav;
%cup_gamNAV_dictAVG = complex_col2im( dict_NAV*gam_AVG, rsize, params.pshape, params.sshape)*maxNav;


gam_NAV_wAVG = wrapFOMP( dict_AVG, rows_NAV, params.L, [] , opts);
im_NAV_wAVG = complex_col2im( dict_AVG*gam_NAV_wAVG , rsize , params.pshape , params.sshape)*maxAvg;


recombed = (im_gamNAV_dictAVG+im_NAV)/2;



rerecon_mov = ifftshift(ifftn(im_AVG));
recon_mov = ifftshift(ifftn(kspace_avg));
rerecon_clean = ifftshift(ifftn(im_NAV));
recon_clean = ifftshift(ifftn(kspace_nav));
recon_cup = ifftshift(ifftn(im_gamNAV_dictAVG));
recon_NAV_wAVG = ifftshift(ifftn(im_NAV_wAVG));


%%%%%%%%%%%
% Display %
%%%%%%%%%%%

dispSlice = 20;

%is display available?
if usejava('jvm') && ~feature('ShowFigureWindows')
    
else
    % disp dictionary built AVG
    % rerecon_avg = ((im_AVG));
    
    figure
    %imagesc(abs(rerecon_mov(:,:,dispSlice)));
    imagesc(abs(rerecon_mov));
    title( strcat( 'REBUILT BW SNR: ', num2str(mySNR(rerecon_mov)) ))
    % disp orig AVG
    % recon_mov = ((kspace_avg));
    
    figure
    %imagesc(abs(recon_mov(:,:,dispSlice)));
    imagesc(abs(recon_mov));
    title( strcat( 'BW SNR: ', num2str(mySNR(recon_mov)) ))
    % disp dictionary built NAV
    % rerecon_clean = ((im_NAV));
    
    figure
    %imagesc(abs(rerecon_clean(:,:,dispSlice)));
    imagesc(abs(rerecon_clean));
    title( strcat( 'REBUILT TW SNR: ', num2str(mySNR(rerecon_clean)) ))
    % disp orig NAV
    % recon_clean = ((kspace_nav));
    
    figure
    %imagesc(abs(recon_clean(:,:,dispSlice)));
    imagesc(abs(recon_clean));
    title( strcat( 'TW SNR : ', num2str(mySNR(recon_clean)) ))
    

    figure
    imagesc(abs(recon_cup(:,:)));
    title( strcat( 'COUPLED BW SNR: ', num2str(mySNR(recon_cup)) ))

     figure
    imagesc(abs(recon_NAV_wAVG(:,:)));
    title( strcat( 'REBUILT TW w/ BW dict SNR: ', num2str(mySNR(recon_NAV_wAVG)) ))
    
    
end




%%%%%%%%%%%
% Metrics %
%%%%%%%%%%%

% L2 norms between origs and rebuilts
fprintf('diff between orig TW and REBUILT BW is %d \n',norm(recon_clean(:)-rerecon_mov(:)))
fprintf('diff between orig TW and BW is %d \n',norm(recon_clean(:)-recon_mov(:)))
fprintf('diff between orig TW and REBUILT TW is %d \n',norm(recon_clean(:)-rerecon_clean(:)))
fprintf('diff between orig TW and COUPLED BW is %d \n',norm(recon_clean(:)-recon_cup(:)))

fprintf('REBUILT BW SNR %f \n', mySNR(rerecon_mov))
fprintf('BW SNR %f \n', mySNR(recon_mov))
fprintf('TW SNR %f \n', mySNR(recon_clean))
fprintf('REBUILT TW SNR %f \n', mySNR(rerecon_clean))
fprintf('COUPLED BW SNR %f \n', mySNR(recon_cup))



%%%%%%%%%%%%%
% End Stuff %
%%%%%%%%%%%%%

toc
matlabpool close

save(fname)



