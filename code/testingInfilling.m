clear all
close all
clc

rng(4)

load('../data/tiny_clean.mat')
sscirc = mat2gray(curCircImg(:,:,20));
kspace_nav = fftn(sscirc);

ks = size(kspace_nav);
kspace_nav = kspace_nav + 4*complex( randn(ks), randn(ks) );
recon = mat2gray(abs(ifftn(kspace_nav)));
imagesc(abs(recon));
kspace_nav = fftn(recon);


load('../data/tiny_rand.mat')
kspace_ext = fftn(mat2gray(abs(outImg(:,:,20))));
undersamp = round(rand(size(kspace_ext)));
kspace_ext_under = kspace_ext.*undersamp;

filepath = split_DL_rebuild_functionize(kspace_ext , kspace_nav,  [8 8], [1 1] ,80, 16, .00001, 20, 'infillingtest' );
load(strcat('../results/',filepath))

recon_NAV_wAVG = fftshift(recon_NAV_wAVG);
recon_cup = fftshift(recon_cup);
recon_mov = fftshift(recon_mov);
rerecon_mov = fftshift(rerecon_mov);

figure
imagesc(abs(recon_NAV_wAVG))
figure
imagesc(abs(recon_cup))
figure
imagesc(abs(recon_mov))
figure
imagesc(abs(rerecon_clean))

mse_navwavg = sum((abs(recon_NAV_wAVG(:))-sscirc(:)).^2)/length(recon_NAV_wAVG(:))
10*log10(max(abs(recon_NAV_wAVG(:)))^2/mse_navwavg)

mse_recon = sum((abs(recon(:))-sscirc(:)).^2)/length(recon(:))
10*log10(max(abs(recon(:)))^2/mse_recon)

mse_cup = sum((abs(recon_cup(:))-sscirc(:)).^2)/length(recon_cup(:))
10*log10(max(abs(recon_cup(:)))^2/mse_cup)


mse_rerecon_mov = sum((abs(rerecon_mov(:))-sscirc(:)).^2)/length(rerecon_mov(:))
10*log10(max(abs(rerecon_mov(:)))^2/mse_rerecon_mov)

mse_recon_mov = sum((abs(recon_mov(:))-sscirc(:)).^2)/length(recon_mov(:))
10*log10(max(abs(rerecon_mov(:)))^2/mse_recon_mov)
