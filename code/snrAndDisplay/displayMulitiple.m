function [ snrs ] = displayMulitiple( inputMat,titles, slize )
%DISPLAYMULITIPLE Summary of this function goes here
%   Detailed explanation goes here

snrs = []

[ xFG, yFG, xBG, yBG ] = getROI( inputMat{1} ,slize );
%load prebaked_coords


allMax = -Inf;
allMin = inf;

for i = 1:1:length(inputMat)
    allMax = max([max(max(max(inputMat{i}))), allMax]);
    allMin = min([min(min(min(inputMat{i}))), allMin]);
end


for i = 1:1:length(inputMat)
    
    snrs(end+1) = displaySlice(inputMat{i},slize, xFG, yFG, xBG, yBG,allMax,allMin,titles{i});
    
    
    
end


end

