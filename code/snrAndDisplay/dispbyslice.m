function [ output_args ] = dispbyslice( inputt ,rescaleEach )
%DISPBYSLICE Summary of this function goes here
%   Detailed explanation goes here

maxx = abs(max(max(max(inputt))))
minn = abs(min(min(min(inputt))))
for i = 1:size(inputt,3)
    imagesc(rot90(rot90(abs(inputt(:,:,i)))));
    if ~rescaleEach
    caxis([minn maxx]);
    end
    title(strcat(num2str(i),'/',num2str(size(inputt,3))))
    waitforbuttonpress 
    
end


waitforbuttonpress
close 

