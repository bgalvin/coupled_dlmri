function [ xFG, yFG, xBG, yBG ] = getROI( inMat1,slize )
%GETROI Summary of this function goes here
%   Detailed explanation goes here
opengl software

[m, n, p] = size(inMat1);

if length(size(inMat1)) == 3
imagesc(inMat1(:,:,slize));
else
    imagesc(inMat1);
end
    
title('Choose upper left and lower right corners of forground and then background')
colormap(gray);
brighten(.6)
hold on

[xFG,yFG] = ginput(2);
fgp = patch([xFG(1) xFG(2) xFG(2) xFG(1)],[yFG(1) yFG(1) yFG(2) yFG(2)],'r');
set(fgp,'FaceAlpha',0.1);
text( xFG(2),yFG(1), 'FOREGROUND', 'Color', 'r');

[xBG,yBG] = ginput(2);
bgp = patch([xBG(1) xBG(2) xBG(2) xBG(1)],[yBG(1) yBG(1) yBG(2) yBG(2)],'g');
set(bgp,'FaceAlpha',0.1);
text( xBG(2),yBG(1), 'BACKGROUND', 'Color', 'g');



end

