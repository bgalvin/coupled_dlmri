function ssim = get_ssim(a,b,slice)

if length(size(a)) == 3
    aa = mat2gray(abs(a(:,:,slice)))*255;
    bb = mat2gray(abs(b(:,:,slice)))*255;
    ssim = ssim_index(aa,bb);
else
    aa = mat2gray(abs(a))*255;
    bb = mat2gray(abs(b))*255;
    ssim = ssim_index(aa,bb);
end


end