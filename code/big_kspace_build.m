clear all
close all
clc

try
 matlabpool close
catch
  matlabpool
end
    

   
%%%%%%%%%%%%%%%%%%%%
% Loading and Path %
%%%%%%%%%%%%%%%%%%%%

addpath('./utils')
load('../data/big_kspace.mat')

% 256     5    33   864
[x, numbc, y, z]=size(all_coils_kSpace);

%nc = 1;

%curcoil = squeeze(all_coils_kSpace(:,nc,:,:));
all_aqui = 1:1:size(all_coils_kSpace,4);
dont_use_theseGates = setdiff(all_aqui,use_theseGates);

ingate    = zeros(x,1);
outofgate = zeros(x,1);

for i = 1:864
    for j = 1:33
        if ismember(i,use_theseGates)
            ingate(:,end) =  squeeze(all_coils_kSpace(:,nc,j,i));
            ingate = [ingate zeros(x,1)];
        else
            outofgate(:,end) =  squeeze(all_coils_kSpace(:,nc,j,i));
            outofgate = [outofgate zeros(x,1)];
        end
    end
end



params.displayProgress = 1 ;
% dictionary size
params.K = 5000;
% KSVD iterations
params.numIteration = 10;
% Determins the use of L or ERRORGOAL, below...
params.errorFlag = 0;
%  0 - fix number of coefficients is used for representation of each signal. param.L
params.L = 20;
%  1 - until a specific representation error is reached. param.errorGoal
params.errorGoal = .0001;

params.preserveDCAtom = 0;

params.InitializationMethod = 'GivenMatrix';

%%%%%%%%%%%%%%
% Algorithm  %
%%%%%%%%%%%%%%


fname = sprintf('trying_SAMPS.mat')

params

disp('starting algo')

tic;
% get orig size


% Normalize kspaces
maxingate = max(max(max(ifftn(ingate))));
maxoutofgate = max(max(max(ifftn(outofgate)))); %peak intensity estimated from zero-filled result

ingate_incols = ingate%./maxingate;
outofgate_incols = outofgate%./maxoutofgate;

toUseIdx = randperm(size(ingate_incols,2));
params.initialDictionary = ingate_incols(:,toUseIdx(1:params.K));
% move NAV to rows, KSVD, and rebuild nav

[dict_ingate, ksvdopts] = KSVDC2(ingate_incols, params, params.errorGoal);
rebuilt_ingate = dict_ingate*ksvdopts.CoefMatrix%.*maxingate;

opts.slowMode = 0;
opts.printEvery = 0;
opts.ignoreCorrupted = 0;
gam_outofgate = wrapFOMP( dict_ingate, outofgate_incols, params.L, [] , opts);

rebuilt_outofgate = dict_ingate*gam_outofgate;%.*maxoutofgate;


save(fname)



