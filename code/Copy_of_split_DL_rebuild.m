clear all
close all
clc

addpath('./utils')

%load('./data/data.mat')
load('./data/tiny_clean.mat')
load('./data/tiny_rand.mat')



%forcirc
kspace_nav = clean_kspace;
kspace_avg = kspace;
% kspace_nav = complex(randn(10,10,10),randn(10,10,10));
% kspace_avg = complex(randn(10,10,10),randn(10,10,10));

profile on

pshape = [12 12 12];
sshape = [1 1 1];

params.displayProgress = 1 ;
params.K = 20;
params.numIteration =10;

%  errorFlag...if =0, a fix number of coefficients is used for representation of each signal. param.L 
%  if =1, arbitrary number of atoms represent each signal, until a specific representation error is reached. param.errorGoal
params.errorFlag =0;
params.L = 20;
params.errorGoal = .01;

params.preserveDCAtom = 0;
params.InitializationMethod = 'DataElements';


maxNav = max(max(max(ifftn(kspace_nav))));  
maxAvg = max(max(max(ifftn(kspace_avg)))); %peak intensity estimated from zero-filled result
kspace_nav=kspace_nav./maxNav;
kspace_avg=kspace_avg./maxAvg;

rsize = size(kspace);

% ks_real = real(kspace_nav);
% ks_imag = imag(kspace_nav);
% 
% RC = im2colstep(ks_real, pshape, sshape);
% IC = im2colstep(ks_imag, pshape, sshape);
% complexrows = complex(RC,IC);

complexrows = complex_im2col( kspace_nav, pshape, sshape );
disp('to complex rows')

[dict,opts] = KSVDC2(complexrows,params,params.errorGoal);
rebuild_clean = dict*opts.CoefMatrix;

% outRC_clean = real(rebuild_clean);
% outIC_clean = imag(rebuild_clean);
% reoutRC_clean = col2imstep(outRC_clean, rsize, pshape, sshape);
% reoutIC_clean = col2imstep(outIC_clean, rsize, pshape, sshape);
% cnt = countcover(rsize, pshape, sshape);
% reoutRC_clean = reoutRC_clean./cnt;
% reoutIC_clean = reoutIC_clean./cnt;
% combinedRCIC_clean = complex(reoutRC_clean, reoutIC_clean);

combinedRCIC_clean = complex_col2im( rebuild_clean, rsize, pshape, sshape  );

avg_comp = complex_im2col( kspace_avg, pshape, sshape );
disp('to complex rows')

%gamma_real = OMP(dict,avg_comp,15);
gamma_real = OMPerrn(dict,avg_comp,params.errorGoal,params.L); 
rebuild = dict*gamma_real;

% outRC_avg = real(rebuild);
% outIC_avg = imag(rebuild);
% reoutRC_avg = col2imstep(outRC_avg,rsize,pshape, sshape);
% reoutIC_avg = col2imstep(outIC_avg,rsize,pshape, sshape);
% reoutRC_avg = reoutRC_avg./cnt;
% reoutIC_avg = reoutIC_avg./cnt;
% combinedRCIC_avg = complex(reoutRC_avg, reoutIC_avg);

combinedRCIC_clean = complex_col2im( rebuild, rsize, pshape, sshape  );

rerecon_avg = (ifftn(combinedRCIC_avg));
figure
imagesc(abs(rerecon_avg(:,:,20)));

rerecon_clean = (ifftn(combinedRCIC_clean));
figure
imagesc(abs(rerecon_clean(:,:,20)));

recon_mov = ifftn(kspace_avg);
figure
imagesc(abs(recon_mov(:,:,20)));

recon_clean = ifftn(kspace_nav);
figure
imagesc(abs(recon_clean(:,:,20)));

norm(recon_clean(:)-rerecon_avg(:))
norm(recon_clean(:)-recon_mov(:))

profile viewer