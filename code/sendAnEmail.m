function [] = sendAnEmail(runName, emailBodyParams)

% Define these variables appropriately:
mail = 'bennerthanyou@gmail.com'; %Your GMail email address
password = 'gmailstormraven';  %Your GMail password
setpref('Internet','SMTP_Server','smtp.gmail.com');

%mystring = evalc('disp(emailBodyParams)');
subj = sprintf('%s has finished',runName);
bod = sprintf('Hello! MATLAB has recently finished a computation! \n %s',evalc('disp(emailBodyParams)'));
% Apply prefs and props
setpref('Internet','E_mail',mail);
setpref('Internet','SMTP_Username',mail);
setpref('Internet','SMTP_Password',password);
props = java.lang.System.getProperties;
props.setProperty('mail.smtp.auth','true');
props.setProperty('mail.smtp.socketFactory.class', 'javax.net.ssl.SSLSocketFactory');
props.setProperty('mail.smtp.socketFactory.port','465');
% Send the email.  Note that the first input is the address you are sending the email to
sendmail(mail, subj, bod)