close all
clear all


%%%%%%%%%%%%%
% Data Prep %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%warning off
subid = 'MID227';
spectit = 'quickTest'
displayRes = 0;

load( strcat('../data/',subid,'_resized_coils.mat'));

%kspace_avg1 = kSpace2g;
% recon_mov12 = ifftshift(ifftn(kSpace2g(:,:,10)));
%kspace_avg1 = (fftn(ifftshift(recon_mov12(:,:,10))));

%kspace_nav1 = kSpace_extra;
% recon_nav12 = ifftshift(ifftn(kSpace_extra(:,:,10)));
% kspace_nav1 = (fftn(ifftshift(recon_nav12(:,:,10))));

% recon_mov12 = ifftshift(ifftn(kSpace2g));
% kspace_avg1 = (fftn(ifftshift(recon_mov12(:,:,10))));
%
% recon_nav12 = ifftshift(ifftn(kSpace_extra));
% kspace_nav1 = (fftn(ifftshift(recon_nav12(:,:,10))));


%%%%%%%%%%%%%%%
% ParFor Prep %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


try
    matlabpool close
catch
    myCluster = parcluster('local')
    matlabpool(myCluster.NumWorkers)
end




%%%%%%%%%%%%%
% Main call %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%for nc = 1

kspace_nav = eval( strcat('kspace_nav_coil',num2str(nc),'_res'));
kspace_ext = eval( strcat('kspace_avg_coil',num2str(nc),'_res'));


% kspace_nav = kspace_nav1(:,1:301,1:42);
% kspace_ext = kspace_ext1(:,1:301,1:42);

fname = strcat(subid,'_',spectit,'_nc',num2str(nc));

filepath = split_DL_rebuild_functionize( kspace_nav, kspace_ext,  [16 22 10], [16 22 10] , 650,      100,      .00001,   10, fname );

%split_DL_rebuild_functionize( TW,         BW,         ps,ss, dictsize, sparseconst, errorconst,iters, filetitle )

%end


%%%%%%%%%%%%%%%%%%%%%
% Display & Metircs %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if displayRes
    
    load(strcat('../results/',filepath));
    
    dispSlice =30;
    global fg1 fg2;
    
    fg1 = 52;
    fg2 = 252;
    %is display available?
    if usejava('jvm') && ~feature('ShowFigureWindows')
        disp('cannot display results due to local env.')
    else
        displaySlice(rerecon_mov,dispSlice)
        displaySlice(recon_mov,dispSlice)
        displaySlice(rerecon_clean,dispSlice)
        displaySlice(recon_clean,dispSlice)
        displaySlice(recon_cup,dispSlice)
        displaySlice(recon_NAV_wAVG,dispSlice)
        
        
        % L2 norms between origs and rebuilts
        
        fprintf('diff between orig TW and REBUILT BW is %d \n',norm(recon_clean(:)-rerecon_mov(:)))
        fprintf('diff between orig TW and BW is %d \n',norm(recon_clean(:)-recon_mov(:)))
        fprintf('diff between orig TW and REBUILT TW is %d \n',norm(recon_clean(:)-rerecon_clean(:)))
        fprintf('diff between orig TW and COUPLED BW is %d \n',norm(recon_clean(:)-recon_cup(:)))
        
        fprintf('diff between orig BW and REBUILT BW is %d \n',norm(recon_mov(:)-rerecon_mov(:)))
        fprintf('diff between orig BW and COUPLED BW is %d \n',norm(recon_mov(:)-recon_cup(:)))
        fprintf('diff between orig BW and REBUILT TW W/gam BW is %d \n',norm(recon_mov(:)-recon_NAV_wAVG(:)))
        
        fprintf('REBUILT BW SNR %f \n', mySNR(rerecon_mov))
        fprintf('BW SNR %f \n', mySNR(recon_mov))
        fprintf('TW SNR %f \n', mySNR(recon_clean))
        fprintf('REBUILT TW SNR %f \n', mySNR(rerecon_clean))
        fprintf('COUPLED BW SNR %f \n', mySNR(recon_cup))
        fprintf('REBUILT TW W/gam BW SNR %f \n', mySNR(recon_NAV_wAVG))
        
        rerecon_mov_snr = mySNR(recon_cup) ;
        recon_mov_snr =  mySNR(recon_mov);
        recon_clean_snr =  mySNR(recon_clean);
        rerecon_clean_snr = mySNR(rerecon_clean);
        recon_cup_snr =mySNR(recon_cup);
        rerecon_clean_wavg_snr = mySNR(recon_NAV_wAVG);
        
    end
end



%%%%%%%%%%%
% End     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

matlabpool close






